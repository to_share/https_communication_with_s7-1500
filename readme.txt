Example of usage of http(s) connection between python http(s) server and S7-1511TF CPU
Example can be also used in S7-1200 with fw >= 4.4 (According to LHTTP library documentation)

In repository there are two python files:

test.py - simple get method, for connection testing purposes
web.py - server script performing usage of Get, Put and Post methods

To communicate via secured channel bash 'autogen.sh' script is used
Run it via command:

bash autogen.sh 

It automaticlly creates new folder with actual certificates (self-signed). If you want to connect the client/browser with teh server you need to implement RootCA.crt in browser/TIA Portal

The python and bash script should be adjusted to case-specific needs, e.g.:

Server addres
Server Port (remeber about disabling firewall on this port (e.g. sudo ufw allow 8000))
Certificate names and server addres in Bash script
Certificate parameter (Country, Site etc.)
