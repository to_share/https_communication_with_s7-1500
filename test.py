# --- Simple server with only get method --- #
import ssl
from http.server import BaseHTTPRequestHandler, HTTPServer

HOST = '192.168.8.45'
PORT = 8000

class Server(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type','text/html; charset=utf-8')
        self.end_headers()
        self.wfile.write("Hello".encode('utf-8','ignore'))

if __name__ == '__main__':
    httpd = HTTPServer((HOST,PORT), Server)
    print('Server started at: '+ str(HOST) + ':' + str(PORT))
    context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    context.load_cert_chain('certs/server.crt','certs/server.key')
    httpd.socket = context.wrap_socket(httpd.socket,True)
    # deprecated
    # httpd.socket = ssl.wrap_socket(httpd.socket,
    #                                server_side=True,
    #                                certfile='server.crt',
    #                                keyfile='server.key',
    #                                ssl_version=ssl.PROTOCOL_TLS)

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
