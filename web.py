import ssl
from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import ssl

HOST = '192.168.8.45'
PORT = 8000
KEY = '{123cba}'

def ret_json(name,address,port):
    dict={
        "name": name,
        "address": address,
        "port": port,
    }
    return json.dumps(dict, indent=3)

class Server(BaseHTTPRequestHandler):

    # send response method
    def __send_resopnse_and_headers(self,code,type_=None,length=None):
        self.send_response(int(code))
        # send type of file in header
        if type_:
            self.send_header('Content-type',str(type_))
        # send length of file in header
        if length:
            self.send_header('Content-Length', str(length))
        # if type_ or length != None -> end headers
        if type_ or length:
            self.end_headers()

    # get method
    def do_GET(self):
        # check if corect key is sent
        if self.headers['Auth-Bearer'] == KEY:
            # main page
            if self.path == "/":
                file = 'main.html'
                f = open(file).read()
                self.__send_resopnse_and_headers(200,'text/html; charset=utf-8','155')
                self.wfile.write(f.encode('ascii','ignore'))
            # return json
            elif self.path == "/json":
                f = ret_json("BananaPI","192.168.8.45",8000)
                self.__send_resopnse_and_headers(200,'application/json','30')
                self.wfile.write(f.encode('ascii', 'ignore'))
            print(f'Data sent to (get method): {self.client_address[0]} on port: {self.client_address[1]}\n')
        else:
            print('\nAPI Key error')
            self.__send_resopnse_and_headers(401)
            
    # post method
    def do_POST(self):
        # check if corect key is sent
        if self.headers['Auth-Bearer'] == KEY:
            if self.path == '/change':
                content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
                post_data = self.rfile.read(content_length) # <--- Gets the data itself
                data_to_send = "From post method"
                self.__send_resopnse_and_headers(200,'text/html; charset=utf-8','23')
                self.wfile.write(data_to_send.format(self.path).encode('utf-8'))
                print(f'Data recieved (post method): "{post_data.decode("utf-8")}" from {self.client_address[0]} on port {self.client_address[1]}"\n')  # <--- Print data in console
        else:
            print('\nAPI Key error')
            self.__send_resopnse_and_headers(401)

    # put method
    def do_PUT(self):
        # check if corect key is sent
        if self.headers['Auth-Bearer'] == KEY:
            if self.path == '/change':
                data_to_send = 'file created/updated'
                file = 'put.txt'
                length = int(self.headers['Content-Length'])
                with open(file, 'wb') as f:
                    f.write(self.rfile.read(length))
                self.__send_resopnse_and_headers(201,'text/html; charset=utf-8','20')
                self.wfile.write(data_to_send.format(self.path).encode('utf-8'))
                print(f'File {file} created/updated (put method) by: {self.client_address[0]} on port: {self.client_address[1]}\n')
        else:
            print('\nAPI Key error')
            self.__send_resopnse_and_headers(401)

if __name__ == '__main__':
    context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    httpd = HTTPServer((HOST,PORT),Server)
    print('Server started at: '+ str(HOST) + ':' + str(PORT))
    context.load_cert_chain('certs/server.crt','certs/server.key')
    httpd.socket = context.wrap_socket(httpd.socket,
                                       True)
    # deprecated
    # httpd.socket = ssl.wrap_socket(httpd.socket,
    #                                server_side=True,
    #                                certfile='localhost.pem',
    #                                ssl_version=ssl.PROTOCOL_TLS)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
