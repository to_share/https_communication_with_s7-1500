#! bin/bash
echo 'Generating certificates started'

# chceck if directory exists
if [ -d "$PWD/certs" ]; then
  echo 'certs/ exist'
else
  mkdir certs
fi

# genearting config file
echo 'authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
IP.1 = 192.168.8.45' >certs/server.ext

# generating CA certificates
echo 'CA certificates creation'
openssl req -x509 -nodes -new -sha256 -days 1024 -newkey rsa:2048 -keyout certs/RootCA.key -out certs/RootCA.pem -subj "/C=US/CN=Example-Root-CA"
openssl x509 -outform pem -in certs/RootCA.pem -out certs/RootCA.crt

#signing certificates
echo 'Ca certificates sigining'
openssl req -new -nodes -newkey rsa:2048 -keyout certs/server.key -out certs/server.csr -subj "/C=PL/ST=Wielkopolska/L=Poznan/O=ABanana/CN=192.168.8.45"
openssl x509 -req -sha256 -days 1024 -in certs/server.csr -CA certs/RootCA.pem -CAkey certs/RootCA.key -CAcreateserial -extfile certs/server.ext -out certs/server.crt

echo 'Completed Succesfull'
